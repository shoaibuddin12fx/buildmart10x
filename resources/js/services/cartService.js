export default class CartService {

    constructor() {
    }

    getCart() {
        return new Promise(function (resolve) {
            let retrivedCart = localStorage.getItem('cart');
            let cart = JSON.parse(retrivedCart);

            if (cart) {
                resolve(cart);
            } else {
                let _cart = {
                    id: Math.random(),
                    items: [],
                    shipping: 0,
                    subtotal: 0,
                    total: 0,
                }

                let str = JSON.stringify(_cart);
                console.log(str);
                localStorage.setItem('cart', str);
                resolve(_cart);
            }
        });
    }

    saveCart(cart) {
        return new Promise(function (resolve) {
            let str = JSON.stringify(cart);
            console.log(str);
            localStorage.setItem('cart', str);
            resolve(true);
        });
    }

    calculateSubtotal(cart) {
        let total = cart.items.reduce((prevVal, item) => {
            let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
            return prevVal + (p * item.qty);
        }, 0);
        cart.subtotal = total;
        return cart;
    }

    calculateTotal(cart) {
        let total = cart.items.reduce((prevVal, item) => {
            let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
            return prevVal + (p * item.qty);
        }, 0);
        cart.total = total + cart.shipping;
        return cart;
    }
}
