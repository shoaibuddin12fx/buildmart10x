<?php
namespace App\Http\Controllers\API;
use App\Helpers\GlobalHelpers;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\OtpNumber;
use App\Models\UserRole;
use App\Models\Voyager\Entity;
use App\Models\Voyager\Profile;
use App\Models\Voyager\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Helpers\helpers;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function login(Request $request){

        $is_phone = GlobalHelpers::isPhoneNumber($request->email);

        $validations = [
            'password' => 'required|string|min:6',
        ];

        if($is_phone){
            $validations['email'] = 'required|string|min:10|max:10';
        }else{
            $validations['email'] = 'required|string|email|max:255';
        }


        $validator = Validator::make($request->all(), $validations);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first()); //response(['errors'=>$validator->errors()->all()], 422);
        }




        $query = User::query();

        if($is_phone){
            $query->where('phone_number', $request->email);
        }else{
            $query->where('email', $request->email);
        }




        $user = $query->where('id', '!=', 1)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $user = new UserResource($user);
                $response = ['token' => $token, 'user' => $user ];
                return self::success("Login Successful", [ 'data' => $response ]);
            } else {
                return self::failure("Password mismatch");
            }
        } else {
            return self::failure('User does not exist');
        }
    }

    public function updateUserProfile(Request $request)
    {
        $data = $request->all();

        $user = Auth::user();

        $type = $user->type;

        if($type == 'entity'){
            $validations['entity_name'] = 'required|string';
            $validations['cr_number'] = 'required|string';
            $validations['website_url'] = 'required|string';
            $validations['brand_name'] = 'required|string';
            $validations['brand_name_ar'] = 'required|string';
            $validations['years_of_experience'] = 'required|string';
            $validations['entity_name_ar'] = 'required|string';

        }

        if($type == 'individual'){
            $validations['nationality'] = 'required|string';
            $validations['gender'] = 'required|string';
            $validations['date_of_birth'] = 'required|string';
            $validations['identification_type'] = 'required|string';
            $validations['identification_number'] = 'required|string';
        }

        $validator = Validator::make($request->all(), $validations);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }
        if ($data['name'] == ""){
            $data['name'] = $data['first_name'] + " " + $data['last_name'];
        }

            $userObj = [
                'name' => $data['name'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'first_name_ar' => $data['first_name_ar'],
                'last_name_ar' => $data['last_name_ar'],
            ];

        $user->update($userObj);

        if($type == 'entity'){

            $entityObj = [
                'user_id' => $user->id,
                'entity_name' => $data['entity_name'],
                'cr_number' => $data['cr_number'],
                'website_url' => $data['website_url'],
                'brand_name' => $data['brand_name'],
                'brand_name_ar' => $data['brand_name_ar'],
                'years_of_experience' => $data['years_of_experience'],
                'entity_name_ar' => $data['entity_name_ar'],
            ];

            $entity = Entity::updateOrCreate(
                $entityObj,
                ['user_id' => $user->id ]
            );

        }
        if($type == 'individual'){

            $profileObj = [
                'user_id' => $user->id,
                'nationality' => $data['nationality'],
                'gender' => $data['gender'],
                'date_of_birth' => $data['date_of_birth'],
                'identification_type' => $data['identification_type'],
                'identification_number' => $data['identification_number'],


            ];
            $profile = Profile::updateOrCreate(
                $profileObj,
                ['user_id' => $user->id ]
            );

        }


        return self::success("Updated Successfully", ['data' =>  $user] );

    }

    public function register(Request $request)
    {
        $data = $request->all();

        $type = $data['type'];
        $validations = [
            'terms_conditions' => 'integer|min:1',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone_number' => 'required|string|min:10',
            'password' => 'required|string|min:6|required_with:confirm_password',
            'confirm_password' => 'min:6|same:password'

        ];

        if($type == 'entity'){

            $validations['entity_name'] = 'required|string';
            $validations['cr_number'] = 'required|string';
            $validations['website_url'] = 'required|string';

        }

        if($type == 'individual'){
            $validations['nationality'] = 'required|string';
            $validations['gender'] = 'required|string';
            $validations['date_of_birth'] = 'required|string';
            $validations['identification_type'] = 'required|string';
            $validations['identification_number'] = 'required|string';
        }

        $validator = Validator::make($request->all(), $validations);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $userObj = [
            'name' => $data['name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'password' => bcrypt($data['password']),
            'remember_token' => Str::random(10),
            'type' => $type
        ];

        $user = new User($userObj);
        $user->role_id = 2;
        $user->save();

        if($type == 'entity'){

            $entityObj = [
                'user_id' => $user->id,
                'entity_name' => $data['entity_name'],
                'cr_number' => $data['cr_number'],
                'website_url' => $data['website_url'],
            ];

            $entity = new Entity($entityObj);
            $entity->save();

        }

        if($type == 'individual'){

            $profileObj = [
                'user_id' => $user->id,
                'nationality' => $data['nationality'],
                'gender' => $data['gender'],
                'date_of_birth' => $data['date_of_birth'],
                'identification_type' => $data['identification_type'],
                'identification_number' => $data['identification_number'],
            ];

            $profile = new Profile($profileObj);
            $profile->save();
        }


        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $user = new UserResource($user);
        $obj = [
            'user_id' => $user->id,
            'phone_number' => $userObj['phone_number']
        ];
        self::createOTP($obj);
        $response = ['token' => $token, 'user' => $user ];

        return self::success("Login Successful", ['data' => $response ] );

    }

    public function createOTP($data){

        $user = User::where('id', $data['user_id'])->first();
        if(!$user){
            return self::failure("No User(s) exist", ['data' => [] ] );
        }
        $otp = OtpNumber::where(['user_id' => $user->id, 'phone_number' => $data['phone_number']])->first();
        $phone = (int) $user->phone_number;
        if(!$otp){
            $hash = GlobalHelpers::generateOtp();
            $data['phone_number'] = $user->phone_number;
            $data['otp_code'] = $hash;
            $data['user_id'] = $user->id;
            $data['status'] = 0;

            $otp = new OtpNumber($data);

            $otp->save();

            self::sendSMS($phone, $hash);
            return self::success('OTP Created successfully', [ 'data' => $otp ]);

        }
        if($otp->status == 1){
            return self::success('OTP Created successfully', [ 'data' => $otp ]);
        }
        if($otp->status == 0){
            $hash = GlobalHelpers::generateOtp();
            $data['otp_code'] = $hash;
            $data['status'] = 0;
            self::sendSMS($phone, $hash);
            $otp->update($data);
            $otp = OtpNumber::where('id', $otp->id)->first();
            return self::success('OTP Created successfully', [ 'data' => $otp ]);
        }

    }

    public function generateCode(Request $request){

        $data = $request->all();
        $auth = Auth::user();
        $user = User::where('id', $auth->id)->first();
        if(!$user){
            return self::failure("No User(s) exist", ['data' => [] ] );
        }
        $type = $data['type'];
        if ($type == 'phone_number'){
            $hash = GlobalHelpers::generateOtp();
            self::sendSMS($data['phone_number'], $hash);

            return self::success('OTP Created successfully', [ 'otp' => $hash ]);
        }
        if ($type == 'email'){
            $hash = GlobalHelpers::generateOtp();
            self::sendSMS($data['email'], $hash);

            return self::success('OTP Created successfully', [ 'data' => $hash ]);
        }


    }

    public function getProfile(Request $request){

        $user = Auth()::user();
        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){
                $profile = new UserResource($profile);
                return self::success("User Profile", [ 'data' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }

    }



    public function updateProfile(Request $request){
        $data = $request->all();
        $user = \Auth()->user();
        if($user){
            $user->update($data);
            $profile = User::where('id', $user->id)->first();
            $profile = new UserResource($profile);
            return self::success("User Profile", [ 'data' => $profile ] );
        }else{
            return self::failure("No User(s) Exist" );
        }

    }

    public function verifyOTP(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'otp_code' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $otp = $data['otp_code'];


        $user = \Auth()->user();
        $otp_numbers = OtpNumber::all()->where('otp_code', $otp)->where('user_id', $user->id)->first();
        if($otp_numbers){

            return self::success("Otp verified", [ 'data' => $otp_numbers ] );
        }else{
            return self::failure("No Otp Exist" );
        }

    }

    public function forgetPassword(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $email = $data['email'];
        $user = User::where('email', $email)->first();
        if($user){
            $hash = GlobalHelpers::incrementalHash();
            return self::success("Otp Created", [ 'data' => [ 'email_sent' => true, 'code' => $hash ]  ] );
        }else{
            return self::failure("No User Exist", [ 'data' => [ 'email_sent' => false ]  ] );
        }

    }



    public function newPassword(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $password = $data['password'];
        $email = $data['email'];

        $user = User::where('email', $email)->first();
        if($user){
            $user->password = bcrypt($password);
            $user->save();
            return self::success("Password Updated" );
        }else{
            return self::failure("Password Not Updated" );
        }



    }

    public function changePassword(Request $request){

        $data = $request->all();
        $user = Auth::user();
        if($user){
            $validator = Validator::make($request->all(), [
                'old_password' => 'required|string|min:6',
                'new_password' => 'required|string|min:6',
                'new_password' => 'required|string|min:6|required_with:confirm_password',
                'confirm_password' => 'min:6|same:new_password'

            ]);

            if ($validator->fails()) {
                return self::failure($validator->errors()->first());
            };

            $current_password = $data['old_password'];
            $new_password = $data['new_password'];
            if($current_password == $new_password){
                return self::failure("Same Password!", ["data" => null] );

            }

            if(Hash::check($current_password, $user->password)) {
                $user->password = bcrypt($new_password);

                $user->update();
                return self::success("Password Updated", ["data" => $user] );

            }else{
                return self::failure("Password Did Not Matched", ["data" => null] );

            }


        }
        else{
            return self::failure("Password Not Updated" );

        }




    }

    public function getRoles(Request $request){

        $roles = Role::all();

        $obj = [];

        // loop for roles once
        $obj = $roles->filter( function ($item, $key) {
            // return only that item whose role_for has provider_admin
            return Str::contains($item->role_for, 'provider_admin');
        })->values();

        foreach ($obj as $role){
            $role['childs'] = $roles->filter( function ($item, $key) use ($role) {
                return Str::contains($item->role_for, $role['name']);
            })->values();

        }
        return self::success("Roles", [ 'data' => $obj ] );


    }

    public function getCompleteProfile(Request $request){

        $user = Auth::user();

        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){

                $data = [];
                if($profile->type == 'entity' ){
                    $data = Entity::where('user_id', $user->id)->first()->toArray();
                    unset($data['id']);
                }

                if($profile->type == 'individual' ){
                    $data = Profile::where('user_id', $user->id)->first()->toArray();
                    unset($data['id']);
                }


                if(isset($data)){
                    $profile = $profile->toArray();
                    $profile = GlobalHelpers::mergeObjects($profile, $data);
                }

                return self::success("User Profile", [ 'data' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist last" );
        }



    }

    public function changeEmailOrContact(Request $request){

        $user = Auth::user();
        if(!$user){
            return self::failure('No User found');
        }

        $data = $request->all();

        $obj = [
            $data['type'] => $data[$data['type']]
        ];

        $user->update($obj);

        return self::success('Profile Updated');

    }

    public function getUserRole(Request $request){
        $user = Auth::user();
        if($user){
            $roles = UserRole::all()->where('user_id', $user->id)->values();
            return self::success("Roles", ['data' => $roles]);

        }
        else{
            return self::failure("Error", "Unauthenticated");

        }


    }




}
