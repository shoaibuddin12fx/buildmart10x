<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['middleware' => [ 'cors' ] ], function () {
    // ...
    Route::get('menus', 'App\Http\Controllers\API\ApiController@getMenus');
    Route::get('site_logo', 'App\Http\Controllers\API\ApiController@getSiteLogo');
    Route::post('test_otp', 'App\Http\Controllers\API\UserController@createOTP');

    Route::group(['prefix' => 'user'], function () {
        Route::post('login', 'App\Http\Controllers\API\UserController@login');
        Route::post('registeration', 'App\Http\Controllers\API\UserController@register');
        Route::post('forget_password', 'App\Http\Controllers\API\UserController@forgetPassword');
        Route::post('new_password', 'App\Http\Controllers\API\UserController@newPassword');
        Route::get('roles', 'App\Http\Controllers\API\UserController@getRoles');

        Route::group(['middleware' => [ 'auth:api' ] ], function () {
            Route::post('sendOTP', 'App\Http\Controllers\API\UserController@sendOTP');
            Route::post('verifyOTP', 'App\Http\Controllers\API\UserController@verifyOTP');
            Route::get('get_complete_profile', 'App\Http\Controllers\API\UserController@getCompleteProfile');
            Route::post('generate_code', 'App\Http\Controllers\API\UserController@generateCode');
            Route::post('change_password', 'App\Http\Controllers\API\UserController@changePassword');
            Route::post('change_email_or_contact', 'App\Http\Controllers\API\UserController@changeEmailOrContact');
            Route::post('change_password', 'App\Http\Controllers\API\UserController@changePassword');
            Route::get('get_user_role', 'App\Http\Controllers\API\UserController@getUserRole');
        });







    });

    Route::get('fetch_products', 'App\Http\Controllers\API\ProductController@fetchProducts')->name('fetch_products');
    Route::get('generate_product_code', 'App\Http\Controllers\API\ProductController@generateProductCode')->name('generate_product_code');
    Route::get('get_product_by_id', 'App\Http\Controllers\API\ProductController@getProductById')->name('get_product_by_id');

    Route::get('categories', 'App\Http\Controllers\API\CategoryController@getCategories');
    Route::get('category_banners', 'App\Http\Controllers\API\CategoryController@getCategoryBanners');




});



Route::group(['middleware' => ['auth:api', 'cors'] ], function(){
    Route::group(['prefix' => 'user'], function () {
        Route::get('/profile', 'App\Http\Controllers\API\UserController@getProfile');;
        Route::post('/profile', 'App\Http\Controllers\API\UserController@updateProfile');
        Route::post('/updateUserProfile', 'App\Http\Controllers\API\UserController@updateUserProfile');


    });
    Route::group(['prefix' => 'company'], function () {
        Route::get('/all', 'App\Http\Controllers\API\CompanyController@getCompanies');
        Route::post('/store', 'App\Http\Controllers\API\CompanyController@store');
        Route::post('/update', 'App\Http\Controllers\API\CompanyController@update');
        Route::get('/get/{id}', 'App\Http\Controllers\API\CompanyController@getCompanyById');
        Route::delete('/delete/{id}', 'App\Http\Controllers\API\CompanyController@deleteCompanyById');
        Route::post('add/users/{company_id}', 'App\Http\Controllers\API\CompanyController@addUsersByCompanyId');
    });
    Route::group(['prefix' => 'subscription'], function () {


        Route::post('/assign', 'App\Http\Controllers\API\SubscriptionController@assign');
        Route::post('/unassign', 'App\Http\Controllers\API\SubscriptionController@unassign');
        Route::get('/subscription_details', 'App\Http\Controllers\API\SubscriptionController@subscriptionDetails');

        Route::post('/update', 'App\Http\Controllers\API\CompanyController@update');
        Route::get('/get/{id}', 'App\Http\Controllers\API\CompanyController@getCompanyById');
        Route::delete('/delete/{id}', 'App\Http\Controllers\API\CompanyController@deleteCompanyById');
        Route::post('add/users/{company_id}', 'App\Http\Controllers\API\CompanyController@addUsersByCompanyId');

        Route::get('fetchSubscriptionPlan','App\Http\Controllers\API\SubscriptionPlanController@index');
        Route::get('save_entity_details', 'App\Http\Controllers\API\SubscriptionPlanController@saveEntityDetails');

    });
});

Route::group(['middleware' => ['cors'] ], function(){

    Route::group(['prefix' => 'subscription'], function () {
        Route::get('/all', 'App\Http\Controllers\API\SubscriptionController@getSubscriptions');
        Route::get('/payment_methods', 'App\Http\Controllers\API\SubscriptionController@getPaymentMethods');
    });

});



