<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    public function getMenus(Request $request){
        $menus = menu('Home page top menu', '_json')->where('menu_id', 2)->transform(function ($item, $key) {
            return $item;
        });


        return self::success('Success', ['data' => $menus]);
    }

    public function getSiteLogo(Request $request){
        $sitelogo = asset('storage/' . setting('site.logo'));

        return self::success('Success', ['data' => $sitelogo]);
    }
}
