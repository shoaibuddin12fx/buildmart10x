<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Models\OtpNumber;

use Illuminate\Support\Facades\Storage;

class GlobalHelpers
{
    public static function incrementalHash($num = 6) {
        $digit_random_number = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999) ;
        return $digit_random_number;
    }

    public static function generateOtp(){
        $hash = GlobalHelpers::incrementalHash();
        while(OtpNumber::where('otp_code', $hash)->first()){
            $hash = GlobalHelpers::incrementalHash();
        }

        return $hash;
    }

    public static function isPhoneNumber($pn)
    {

        if( filter_var($pn, FILTER_VALIDATE_EMAIL)){
            return false;
        }

        if( !is_null($pn) ){

            $pn = preg_replace('/\D/', '', $pn);
            $pn = substr($pn, -10);

            if(strlen($pn) < 10){
                return false;
            }

            return true;

        }

        return false;
    }

    public static function mergeObjects($object1, $object2)
    {

        $merge_object = (object) array_merge((array) $object1, (array) $object2);

        $p = [];
        foreach($merge_object as $property => $value) {
            $p[$property] = $value ;
        }
        return $p;
    }


}



