<?php

namespace App\Models\Voyager;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPaymentCard extends Model
{
    use HasFactory;
}
