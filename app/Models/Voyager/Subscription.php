<?php

namespace App\Models\Voyager;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
	use HasFactory;

	protected $guarded = [];

	public function prices()
	{
		return $this->hasMany(SubscriptionPrice::class, 'subscription_id');
	}
}
