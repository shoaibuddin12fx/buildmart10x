<?php

namespace App\Models\Voyager;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPrice extends Model
{
	use HasFactory;
	protected $guarded = [];

	public function price()
	{
		return $this->belongsTo(Subscription::class);
	}
}
