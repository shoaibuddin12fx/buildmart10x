/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueRouter from "vue-router";
import Loading from 'vue-loading-overlay';
import VueMq from 'vue-mq';
import VueI18n from 'vue-i18n'
import SqliteService from "./services/sqlite.service";
import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
import OtpInput from "@bachdgvn/vue-otp-input";

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component("v-otp-input", OtpInput);


let en = require('../lang/en.json');
let ar = require('../lang/ar.json');
var messages = {
    "en": en,
    "ar": ar
} // require('./config/lang.json');
require('./bootstrap');

// import routes from "./routes";


// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


import {routes} from "./routes";

window.Vue = require('vue').default;
Vue.use(VueRouter);
Vue.use(VueI18n);
Vue.mixin(require('./asset'));
import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)|
Vue.use(Loading, {
    isFullPage: true,
    loader: 'spinner'
});
Vue.use(VueMq, {
    breakpoints: {
        sm: 450,
        md: 1250,
        lg: Infinity,
    }
});



// Create VueI18n instance with options











const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
})


// points to add

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('app-component', require('./views/App.vue').default);
require("./components");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    base: '/buildmart10x/public',
    routes: routes,
    mode: "history"
})

let sqlite = new SqliteService();
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCTkeMDqYM4gUHkcl7b7-mh-bWvEpFUNmY',
        libraries: 'places',
    }
});

sqlite.initialize().then( v => {
    window.onload = function () {
        const app = new Vue({
            el: '#app',
            components: { Multiselect },
            i18n,
            router
        });
    }
})


