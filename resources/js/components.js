// Declare All the Components Here

Vue.component('top-header', require('./components/header/TopHeaderComponent').default);
Vue.component('main-menu', require('./components/header/MenuComponent/MenuComponent').default);
Vue.component('nav-search', require('./components/header/HeaderSearch/HeaderSearchComponent').default);
Vue.component('header-meta', require('./components/header/HeaderMeta/HeaderMetaComponent').default);
Vue.component('header-bar-categories', require('./components/header/HeaderBarCategories/HeaderBarCategories').default);
Vue.component('header-slider', require('./components/header/HeaderSlider/HeaderSlider').default);
Vue.component('main-footer', require('./components/footer/FooterComponent').default);
Vue.component('locale-changer', require('./components/header/MenuComponent/LocaleChanger').default);
Vue.component('sign-in-page', require('./pages/signin-page/SignInPageComponent').default);
Vue.component('why-us', require('./components/home/why-us/WhyUsComponent').default);
Vue.component('home-carousel', require('./components/home/home-page-carousel/HomePageCarouselComponent').default);
Vue.component('shop-by-category', require('./components/home/shop-by-category/ShopByCategoryComponent').default);
Vue.component('brands', require('./components/home/brands/BrandsComponent').default);
Vue.component('hot-offers', require('./components/home/hot-offers/HotOffersComponent').default);
Vue.component('home-categories', require('./components/home/categories/CategoriesComponent').default);
Vue.component('home-section-names', require('./components/home/section-names/SectionNameComponent').default);
Vue.component('home-banner', require('./components/home/home-banner/HomeBannerComponent').default);
Vue.component('coming-soon', require('./components/home/coming-soon/ComingSoonComponent').default);

// updated a few

// Vue.component('subscription-packages', require('./pages/profile-page/subscription-packages/SubscriptionPackageComponent').default);
Vue.component('subscription-plans', require('./pages/subscription-plan/SubscriptionPlanComponent').default);
Vue.component('user-profile', require('./pages/profile-page/user-profile/UserProfileComponent').default);
Vue.component('header-second', require('./components/header/HeaderSecond/HeaderSecond').default)
Vue.component('sellers-page', require('./pages/sellers-page/SellersPage').default)
Vue.component('serviceprovider-page', require('./pages/serviceprovider-page/ServiceProviderPageComponent').default)
Vue.component('user-entity-profile', require('./pages/profile-page/user-profile/UserEntityProfileComponent').default);
Vue.component('user-individual-profile', require('./pages/profile-page/user-profile/UserIndividualProfileComponent').default);
Vue.component('subscription-page', require('./pages/profile-page/subscription-packages/SubscriptionComponent').default);
Vue.component('subscription-information', require('./pages/subscription-plan/SubscriptionInformation').default);
Vue.component('line-item-header', require('./components/header/LineItemHeader/LineItemHeader').default);
Vue.component('subscription-sections', require('./pages/subscription-plan/SubscriptionSections').default);
Vue.component('subscription-addresses', require('./pages/subscription-plan/SubscriptionAddresses').default );
Vue.component('subscription-plan', require('./pages/subscription-plan/SubscriptionPayments'));
Vue.component('subscription-packages', require('./pages/subscription-plan/SubscriptionPackages').default);
Vue.component('subscription-map-address', require('./pages/subscription-plan/SubscriptionMapAddress').default);
// modals
Vue.component('login-modal', require('./components/modals/LoginModal/LoginModal').default);
Vue.component('register-modal', require('./components/modals/RegisterModal/RegisterModal').default);
Vue.component('verificationCode-modal', require('./components/modals/RegisterModal/VerificationModal').default);
Vue.component('changePassword-modal', require('./components/modals/LoginModal/ChangePasswordModal').default);
// Vue.component('changeContact-modal', require('./components/modals/LoginModal/ChangePasswordModal').default);
