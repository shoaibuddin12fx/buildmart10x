<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        .vld-overlay.is-active {
            position: absolute;
            left: 50%;
            top: 50%;
            right: 50%;
            bottom: 50%;
            width: 100%;
            height: 100%;
            z-index: 9999;
        }

        .phone-input-country-wise{
            display: inline-block;
            width: 95%;
            height: calc(1.6em + 0.75rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #495057;
            background-color: #E4E5E6 !important;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .otp-input {
            width: 50px;
            height: 50px;
            padding: 5px;
            margin: 0 10px;
            font-size: 2.5rem;
            outline: 0;
            border-width: 0px 0px 1px 0px;
            border-radius: 0;
            text-align: center;
            border-color: rgb(231,231,232);
            color: rgb(31,43,123);
        }
        .otp-input.error {
            border: 1px solid red !important;
        }
        .otp-input::-webkit-inner-spin-button, .otp-input::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }


    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    <script>
        var laravel = @json([ 'baseURL' => url('/'), 'csrfToken' => csrf_token()  ])
    </script>

    <script>
        window._asset = '{{ asset('') }}';
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="v-toaster/dist/v-toaster.css"></link>
    <script src="v-toaster/dist/v-toaster.js"></script>
    <style src="vue-multiselect/dist/vue-multiselect.min.css"></style>

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div id="app" class="milage">

    <router-view></router-view>
    <div class="row">
        <div class="col-12" style="background-color: #F2F2F2;">
            <main-footer></main-footer>
        </div>
    </div>
    <login-modal ></login-modal>



</div>
</body>
</html>
