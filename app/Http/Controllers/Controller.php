<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public static function success($msg, $data = [], $code = 200){

        $msgArray = array(
            'bool' => true,
            'status' => $code,
            "message" => $msg
        );

        $returnArray = array_merge($msgArray, $data);
        // Log::info('app.requests', ['type'=> 'success', 'request' => request()->all(), 'response' => $msg]);
        return response()->json( $returnArray, $code);
    }

    public static function failure($error, $data = [], $code = 422 ){

        $msgArray = array(
            'bool' => false,
            'status' => $code,
            "message" => $error
        );

        $returnArray = array_merge($msgArray, $data);
        // Log::info('app.requests', ['type'=> 'error', 'request' => request()->all(), 'response' => $error]);
        return response()->json( $returnArray, $code);


    }

    public function sendSMS($number,$message){
        $number_rcvd = $number;
        $msg_rcvd = $message;
        $username   = USERNAME;
        $password   = PASSWORD;
        $sender     = SENDER;
        $number     = $number_rcvd;
        $message    = $msg_rcvd;
        $date       = date('Y-m-d');
        $time       = date('H:i');

        $url = "https://www.hisms.ws/api.php?send_sms&username=".$username."&password=".$password."&numbers=".$number."&sender=".$sender."&message=".$message."&date=".$date."&time=".$time;


        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
        // curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        $result = curl_exec($ch );
        if (curl_errno($ch) > 0) {
            print 'There was a cURL error: ' . $curl_error($ch);
        }
        curl_close( $ch );

        $final_result = substr($result, 0, 1);

        if($final_result == 3){
            return 'true';
        }else{
            return 'false';

        }
    }
}
