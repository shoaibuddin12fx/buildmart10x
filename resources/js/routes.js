import SqliteService from "./services/sqlite.service";
import NetworkService from "./services/network.service";
import UtilityService from "./services/utility.service";


let sqlite = new SqliteService();
let network = new NetworkService();
let utility = new UtilityService();

// routes js to maintain

async function authGuard(to, from, next) {
    var isAuthenticated = false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if (user) {
        next(); // allow to enter route
    } else {
        next('/sign-in'); // go to '/login';
    }
}

async function inverseAuthGuard(to, from, next) {
    var isAuthenticated = false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if (!user) {
        next(); // allow to enter route
    } else {
        next('/profile'); // go to '/login';
    }
}

async function authToSubscription(to, from, next) {
    var isAuthenticated = false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let roles = await network.getUserRole();
    let auth = roles.filter((x) => x.role_id >= 17)
    if (auth) {
        utility.presentFailureToast('You do not have access to this page, subscription already completed!')
        next('/profile');
    } else {
        next(); // go to '/login';
    }
}

export const routes = [
    { path: '/', component: require('./pages/home-page/HomePageComponent').default },
    { path: '/sign-in', component: require('./pages/signin-page/SignInPageComponent').default, beforeEnter: inverseAuthGuard },
    { path: '/profile', component: require('./pages/profile-page/ProfilePageComponent').default, beforeEnter: authGuard },
    { path: '/sellers-page', component: require('./pages/sellers-page/SellersPage').default },
    { path: '/subscription-plan', component: require('./pages/subscription-plan/SubscriptionPlanComponent').default },
    { path: '/service-provider', component: require('./pages/serviceprovider-page/ServiceProviderPageComponent').default }
];
