<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Voyager\Category;
use App\Models\Voyager\CategoryBanner;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function getCategories(Request $request){

        $categories = Category::whereNull('parent_id')->get();
        return self::success('Categories', [ 'data' => [ 'categories' => $categories ] ]);

    }

    public function getCategoryBanners(Request $request){

        $banners = CategoryBanner::all();
        return self::success('Category Banners' , [ 'data' => [ 'banners' => $banners ] ] ) ;

    }

}
