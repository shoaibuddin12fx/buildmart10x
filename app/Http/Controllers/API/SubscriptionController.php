<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\SubscriptionCollection;
use App\Http\Resources\SubscriptionResource;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\Voyager\CompanySubscription;
use App\Models\Voyager\PaymentMethod;
use App\Models\Voyager\Role;
use App\Models\Voyager\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    //
    public function getSubscriptions(Request $request){

        $subscriptions = Subscription::with('prices')->get();
        return self::success('Subscriptions', ['data' => $subscriptions]);
    }

    public function assign(Request $request){

        $validators = Validator::make($request->all(), [
            'subscription_id' => 'required|exists:subscriptions,id',
        ] );

        if ($validators->fails())
        {
            return self::failure($validators->errors()->first());
        }

        $userId = Auth::user()->id;
        $data = $request->all();

        $cc = new CompanyController();
        $company_name = $userId . Carbon::now()->timestamp;
        $request->request->add([
            'name' => $company_name
        ]);
        $company = $cc->store($request)->getData()->data;
        $subscription = new SubscriptionResource(Subscription::where('id', $data['subscription_id'] )->first());

        $obj = [
            'company_id' => $company->id,
            'subscription_id' => $data['subscription_id']
        ];
        CompanySubscription::updateOrCreate($obj, $obj);

        // check what is the type of subscription ??
        // if type is company .... check for company_admin role
        // if type is provider ... check for provider_admin role
        // remaing all code as is

        $roleType = $subscription['type'] == 'company' ? 'company_admin' : 'provider_admin';
        $role = Role::where('name' , $roleType )->first();
        if($role){
            Auth::user()->roles()->sync($role);
        }

        return self::success('Subscriptions', [ 'data' => [ 'company' => $company, 'subscription' => $subscription ] ] );
    }

    public function subscriptionDetails(Request $request){

        $userId = Auth::user()->id;
        // check if user subscribes to a company
        $companyUser = CompanyUser::where('user_id', $userId)->count();
        $isUserSubscribed = false;
        $isUserSubscribedOwner = false;
        $company = null;
        $subscription = null;
        $plist = [];

        if($companyUser > 0){

            $company_ids = CompanyUser::where('user_id', $userId)->pluck('company_id');
            $subs = CompanySubscription::whereIn('company_id', $company_ids)->get();


            if($subs->count() > 0){


                $plist = $subs->transform(function ($li){

                    $company = Company::where('id', $li->company_id)->first();
                    if($company){
                        $company = new CompanyResource($company);
                    }

                    $subscription = Subscription::where('id', $li->subscription_id)->first();
                    if($subscription){
                        $subscription = new SubscriptionResource($subscription);
                    }

                    return [
                        'li' => $li,
                        'company' => $company,
                        'subscription' => $subscription
                    ];

                });

                $isUserSubscribed = true;
                $isUserSubscribedOwner = true;

            }

        }

        return self::success('Subscription Details', [ 'data' => [ 'plist' => $plist, 'is_user_subscribed' => $isUserSubscribed, 'is_user_subscribed_owner' => $isUserSubscribedOwner ] ]);


    }

    public function unassign(Request $request){

        $data = $request->all();
        $li = $data['li'];
        if($li){
            // from the id of li
            // mark li in database as soft delete and remove the item from here
            CompanyUser::where('id', $li['id'])->delete();
            $role = Role::where('name', 'user')->first();
            if($role){
                Auth::user()->roles()->sync($role);
            }

        }

        return self::success('Subscription Unassigned', [ 'data' => $li ]);
    }

    public function isUserSubscriptionLimitOver(){

        $response = $this->subscriptionDetails(\request())->getData();
        $data = $response->data;
//        dd($data);

        $obj = [
            'canAddUser' => true,
            'remaining_limit' => -1,
        ];

        if($data->is_user_subscribed == true){

            $plist = $data->plist;

            $user_limit = $plist[0]->subscription->user_limit;
            $user_count = CompanyUser::where('company_id', $plist[0]->company->id)->count();

            $obj['remaining_limit'] = $user_limit - $user_count;

            if($user_limit < 0){
                $obj['canAddUser'] = false;
            }
            if($user_count >= $user_limit){
                $obj['canAddUser'] = true;
            }else{
                $obj['canAddUser'] = false;
            }

        }

        return $obj;




    }

    public function addVoyagerUserToCompany($authId, $userId){

        $company = Company::where('created_by', $authId)->first();
        if($company){

            $obj = [
                'company_id' => $company->id,
                'user_id' => $userId
            ];
            CompanyUser::updateOrCreate($obj, $obj);
        }

    }

    public function getPaymentMethods(Request $request){

        // do all the code here
        $payment = PaymentMethod::all();

        return self::success("payment mehtods", [ 'data' => $payment ]);

    }
}
